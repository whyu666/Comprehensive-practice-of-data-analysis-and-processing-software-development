import request from '@/utils/request'

// 查询展示bangumi评论列表
export function listBangumicomment(query) {
  return request({
    url: '/bangumicomment/bangumicomment/list',
    method: 'get',
    params: query
  })
}

export function listBangumicomment2(query) {
  return request({
    url: '/bangumicomment/bangumicomment/listBangumicomment2',
    method: 'get',
    params: query
  })
}

export function bangumianddoubanall(query) {
  return request({
    url: '/bangumicomment/bangumicomment/getbangumiall',
    method: 'get',
    params: query
  })
}

// 查询展示bangumi评论详细
export function getBangumicomment(id) {
  return request({
    url: '/bangumicomment/bangumicomment/' + id,
    method: 'get'
  })
}

// 新增展示bangumi评论
export function addBangumicomment(data) {
  return request({
    url: '/bangumicomment/bangumicomment',
    method: 'post',
    data: data
  })
}

// 修改展示bangumi评论
export function updateBangumicomment(data) {
  return request({
    url: '/bangumicomment/bangumicomment',
    method: 'put',
    data: data
  })
}

// 删除展示bangumi评论
export function delBangumicomment(id) {
  return request({
    url: '/bangumicomment/bangumicomment/' + id,
    method: 'delete'
  })
}
