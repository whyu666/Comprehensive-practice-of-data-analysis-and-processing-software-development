import request from '@/utils/request'

// 查询展示界面列表
export function listSubject(query) {
  return request({
    url: '/subject/subject/list',
    method: 'get',
    params: query
  })
}

// 查询展示界面详细
export function getSubject(rk) {
  return request({
    url: '/subject/subject/' + rk,
    method: 'get'
  })
}

