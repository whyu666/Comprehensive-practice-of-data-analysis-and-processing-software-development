import request from '@/utils/request'

// 查询bangumi+douban列表
export function listDoublecomment(query) {
  return request({
    url: '/doublecomment/doublecomment/list',
    method: 'get',
    params: query
  })
}

// doubancomment API
export function listDoubancomment(query) {
  return request({
    url: '/doublecomment/doublecomment/listDoubancomment',
    method: 'get',
    params: query
  })
}
// bangumicomment score+quantity
export function listBangumicomment2(query) {
  return request({
    url: '/doublecomment/doublecomment/listBangumicomment2',
    method: 'get',
    params: query
  })
}
// doubancomment score+quantity
export function listDoubancomment2(query) {
  return request({
    url: '/doublecomment/doublecomment/listDoubancomment2',
    method: 'get',
    params: query
  })
}
// 是的详细展示页面再显示一下，要不然进去就不知道是哪部作品了，作品名也显示一下
export function bangumianddoubanall(query) {
  return request({
    url: '/doublecomment/doublecomment/getbangumiall',
    method: 'get',
    params: query
  })
}


// 查询bangumi+douban详细
export function getDoublecomment(id) {
  return request({
    url: '/doublecomment/doublecomment/' + id,
    method: 'get'
  })
}

// 新增bangumi+douban
export function addDoublecomment(data) {
  return request({
    url: '/doublecomment/doublecomment',
    method: 'post',
    data: data
  })
}

// 修改bangumi+douban
export function updateDoublecomment(data) {
  return request({
    url: '/doublecomment/doublecomment',
    method: 'put',
    data: data
  })
}

// 删除bangumi+douban
export function delDoublecomment(id) {
  return request({
    url: '/doublecomment/doublecomment/' + id,
    method: 'delete'
  })
}
