package com.ruoyi.DAPMS.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.DAPMS.domain.BangumiComment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.DAPMS.domain.DoubleComment;
import com.ruoyi.DAPMS.service.IDoubleCommentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * bangumi+doubanController
 *
 * @author 任容、吴春梅
 * @date 2023-11-22
 */
@RestController
@RequestMapping("/doublecomment/doublecomment")
public class DoubleCommentController extends BaseController
{
    @Autowired
    private IDoubleCommentService doubleCommentService;

    /**
     * 查询bangumi+douban列表
     */
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:list')")
    @GetMapping("/list")
    public TableDataInfo list(DoubleComment doubleComment)
    {
        startPage();
//        QueryWrapper<DoubleComment> queryWrapper = new QueryWrapper<>();
        List<DoubleComment> list = doubleCommentService.bangumilist(doubleComment);
        return getDataTable(list);
    }
//    查询doubancomment
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:listDoubancomment')")
    @GetMapping("/listDoubancomment")
    public TableDataInfo listDoubancomment(DoubleComment doubleComment)
    {
        startPage();
        List<DoubleComment> listDouban = doubleCommentService.listDouban(doubleComment);
        return getDataTable(listDouban);
    }
    //查询bangumicomment 评分+评分数
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:listBangumicomment2')")
    @GetMapping("/listBangumicomment2")
    public TableDataInfo listBangumicomment2(DoubleComment doubleComment)
    {
        List<DoubleComment> list2 = doubleCommentService.list2(doubleComment);
        return getDataTable(list2);
    }

    //查询doubancomment 评分+评分数
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:listDoubancomment2')")
    @GetMapping("/listDoubancomment2")
    public TableDataInfo listDoubancomment2(DoubleComment doubleComment)
    {
        List<DoubleComment> listDouban2 = doubleCommentService.listDouban2(doubleComment);
        return getDataTable(listDouban2);
    }

    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:getbangumiall')")
    @GetMapping("/getbangumiall")
    public TableDataInfo getbangumiall(DoubleComment doubleComment)
    {
        List<DoubleComment> getbangumiall = doubleCommentService.getbangumiall(doubleComment);
        return getDataTable(getbangumiall);
    }



    /**
     * 导出bangumi+douban列表
     */
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:export')")
    @Log(title = "bangumi+douban", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DoubleComment doubleComment)
    {
        QueryWrapper<DoubleComment> queryWrapper = new QueryWrapper<>();
        List<DoubleComment> list = doubleCommentService.list(queryWrapper);
        ExcelUtil<DoubleComment> util = new ExcelUtil<DoubleComment>(DoubleComment.class);
        util.exportExcel(response, list, "bangumi+douban数据");
    }

    /**
     * 获取bangumi+douban详细信息
     */
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(doubleCommentService.getById(id));
    }

    /**
     * 新增bangumi+douban
     */
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:add')")
    @Log(title = "bangumi+douban", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DoubleComment doubleComment)
    {
        return toAjax(doubleCommentService.save(doubleComment));
    }

    /**
     * 修改bangumi+douban
     */
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:edit')")
    @Log(title = "bangumi+douban", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DoubleComment doubleComment)
    {
        return toAjax(doubleCommentService.updateById(doubleComment));
    }

    /**
     * 删除bangumi+douban
     */
    @PreAuthorize("@ss.hasPermi('doublecomment:doublecomment:remove')")
    @Log(title = "bangumi+douban", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(doubleCommentService.removeByIds(ids));
    }
}
