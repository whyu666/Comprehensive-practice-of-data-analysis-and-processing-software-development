package com.ruoyi.DAPMS.controller;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.kennycason.kumo.WordFrequency;
import com.ruoyi.DAPMS.domain.Subject;
import com.ruoyi.DAPMS.service.IDiagramCacheService;
import com.ruoyi.DAPMS.service.ISubjectService;
import com.ruoyi.common.annotation.Anonymous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.imageio.ImageIO;
//import jakarta.servlet.http.HttpServletResponse;  // java17
import javax.servlet.http.HttpServletResponse;      // java8
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@RestController
@RequestMapping("/diagrams")
public class DiagramController {

    IDiagramCacheService diagramCacheService;
    ISubjectService subjectService;
    String apiImageFormat;

    @Autowired
    public void setDiagramCacheService(IDiagramCacheService diagramCacheService) {
        this.diagramCacheService = diagramCacheService;
    }

    @Autowired
    public void setSubjectService(ISubjectService subjectService) {
        this.subjectService = subjectService;
    }

    @Value(value = "${tianq.image.api-image-format}")
    public void setApiImageFormat(String apiImageFormat) {
        this.apiImageFormat = apiImageFormat;
    }


    @Anonymous
    @GetMapping("/{sub_id}")
    public void getDiagram(
            @PathVariable("sub_id") String subjectId,
            @RequestParam(value = "min", required = false, defaultValue = "0") int minScore,
            @RequestParam(value = "max", required = false, defaultValue = "10") int maxScore,
            @RequestParam(value = "use_cache", required = false, defaultValue = "true") boolean useCache,
            HttpServletResponse response
    ) throws IOException {

        BufferedImage image;
        if (useCache) image = diagramCacheService.getDiagram(subjectId, minScore, maxScore);
        else image = diagramCacheService.makeDiagramCache(subjectId, minScore, maxScore);

        // 获取失败，可能是ID不存在
        if (Objects.isNull(image)) {

            String msg =
                    "[error]DiagramController::getDiagram" +
                            "(subjectId=" + subjectId + ", minScore=" + minScore + ", maxScore=" + maxScore + ")" +
                            " get null image, check if subjectId exists";
            System.out.println(msg);
            JSONObject result = JSONObject.of("msg", msg);

            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.getWriter().write(result.toString());

            return;
        }

        ByteArrayOutputStream os = new ByteArrayOutputStream();
//        boolean written = ImageIO.write(image, "png", os);
        boolean written = ImageIO.write(image, apiImageFormat, os);

        // 防止图片流写入出错
        if (!written) {
            String msg =
                    "[error]DiagramController::getDiagramNew" +
                            "(subjectId=" + subjectId + ", minScore=" + minScore + ", maxScore=" + maxScore + ")" +
                            " write image failed(" + apiImageFormat + "), check image format";
            System.out.println(msg);
            JSONObject result = JSONObject.of("msg", msg);

            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.getWriter().write(result.toString());

            return;
        }

        // 复制图片输出流到返回
//        response.setContentType(MediaType.IMAGE_PNG_VALUE);
        response.setContentType("image/" + apiImageFormat);
        response.setStatus(HttpServletResponse.SC_OK);
        response.getOutputStream().write(os.toByteArray());

    }

    @Anonymous
    @GetMapping(
            value = "/{sub_id}/frequencies",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public JSONArray getFrequencies(
            @PathVariable("sub_id") String subjectId,
            @RequestParam(value = "min", required = false, defaultValue = "1") int minScore,
            @RequestParam(value = "max", required = false, defaultValue = "10") int maxScore,
            @RequestParam(value = "limit", required = false, defaultValue = "200") int limit,
            @RequestParam(value = "cache_only", required = false, defaultValue = "false") boolean cacheOnly
    ) {
        List<WordFrequency> wordFrequencies = diagramCacheService.getFrequencies(subjectId, minScore, maxScore);
        if (cacheOnly) return null;
        if (Objects.isNull(wordFrequencies) || wordFrequencies.isEmpty() || wordFrequencies.size() <= limit) {
            return JSONArray.from(wordFrequencies);
        }
        return JSONArray.from(wordFrequencies.subList(0, limit));
    }

    @Anonymous
    @GetMapping(
            value = "/{sub_id}/comments",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public JSONArray getComments(
            @PathVariable("sub_id") String subjectId,
            @RequestParam(value = "min", required = false, defaultValue = "1") int minScore,
            @RequestParam(value = "max", required = false, defaultValue = "10") int maxScore
    ) {
        List<String> comments = diagramCacheService.getComments(subjectId, minScore, maxScore);
        return JSONArray.from(comments);
    }

    @Anonymous
    @GetMapping("/cache")
    public SseEmitter cacheFrequencies(
            @RequestParam("from") int rk_a,
            @RequestParam("to") int rk_b,
            @RequestParam(value = "make_image", required = false, defaultValue = "false") boolean makeImage
    ) {
        SseEmitter emitter = new SseEmitter();
        try {

            emitter.send("caching:" + rk_a + "-" + rk_b);

            QueryWrapper<Subject> wrapper = new QueryWrapper<>();
            wrapper.ge("rk", rk_a);
            wrapper.le("rk", rk_b);
            List<Subject> subjects = subjectService.list(wrapper);

            int processors = Runtime.getRuntime().availableProcessors();
            ExecutorService executorService = Executors.newFixedThreadPool(processors);
            for (Subject s : subjects) {
                if (makeImage) {
                    executorService.submit(() -> {
                        diagramCacheService.getDiagram(s.getId(), 1, 5);
                        diagramCacheService.getDiagram(s.getId(), 6, 10);
                        diagramCacheService.getDiagram(s.getId(), 1, 10);
                    });
                } else {
                    executorService.submit(() -> {
                        diagramCacheService.getFrequencies(s.getId(), 1, 5);
                        diagramCacheService.getFrequencies(s.getId(), 6, 10);
                        diagramCacheService.getFrequencies(s.getId(), 1, 10);
                    });
                }

                emitter.send("task submitted:" + s.getId());
            }
            emitter.complete(); // Complete the SSE connection
        } catch (Exception e) {
            emitter.completeWithError(e); // Handle errors
            System.out.println("sse fucked up");
        }
        return emitter;
    }

}