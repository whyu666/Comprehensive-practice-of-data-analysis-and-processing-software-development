package com.ruoyi.DAPMS.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.DAPMS.domain.Subject;
import com.ruoyi.DAPMS.service.ISubjectService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 展示界面Controller
 *
 * @author xyq
 * @date 2023-11-21
 */
@RestController
@RequestMapping("/subject/subject")
public class SubjectController extends BaseController
{
    @Autowired
    private ISubjectService subjectService;

    /**
     * 查询展示界面列表
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:list')")
    @GetMapping("/list")
    public TableDataInfo list(Subject subject)
    {
        startPage();
        List<Subject> list = subjectService.list(subject);
        return getDataTable(list);
    }

    /**
     * 获取展示界面详细信息
     */
    @PreAuthorize("@ss.hasPermi('subject:subject:query')")
    @GetMapping(value = "/{rk}")
    public AjaxResult getInfo(@PathVariable("rk") String rk)
    {
        return success(subjectService.getById(rk));
    }

}
