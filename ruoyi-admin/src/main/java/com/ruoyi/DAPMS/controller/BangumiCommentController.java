package com.ruoyi.DAPMS.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.DAPMS.domain.BangumiComment;
import com.ruoyi.DAPMS.service.IBangumiCommentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 展示bangumi评论Controller
 *
 * @author 吴春梅、任容
 * @date 2023-11-17
 */
@RestController
@RequestMapping("/bangumicomment/bangumicomment")
public class BangumiCommentController extends BaseController
{
    @Autowired
    private IBangumiCommentService bangumiCommentService;

    /**
     * 查询展示bangumi评论列表
     */
    @PreAuthorize("@ss.hasPermi('bangumicomment:bangumicomment:list')")
    @GetMapping("/list")
    public TableDataInfo list(BangumiComment bangumiComment)
    {
        startPage();
        List<BangumiComment> list = bangumiCommentService.listlist(bangumiComment);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('bangumicomment:bangumicomment:list')")
    @GetMapping("/listBangumicomment2")
    public TableDataInfo listBangumicomment2(BangumiComment bangumiComment)
    {
        List<BangumiComment> list2 = bangumiCommentService.listlist2(bangumiComment);
        return getDataTable(list2);
    }


    @PreAuthorize("@ss.hasPermi('bangumicomment:bangumicomment:getbangumiall')")
    @GetMapping("/getbangumiall")
    public TableDataInfo getbangumiall(BangumiComment bangumiComment)
    {
        List<BangumiComment> getbangumiall = bangumiCommentService.getbangumiall(bangumiComment);
        return getDataTable(getbangumiall);
    }

    /**
     * 导出展示bangumi评论列表
     */
    @PreAuthorize("@ss.hasPermi('bangumicomment:bangumicomment:export')")
    @Log(title = "展示bangumi评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BangumiComment bangumiComment)
    {
        List<BangumiComment> list = bangumiCommentService.listlist(bangumiComment);
        ExcelUtil<BangumiComment> util = new ExcelUtil<BangumiComment>(BangumiComment.class);
        util.exportExcel(response, list, "展示bangumi评论数据");
    }

    /**
     * 获取展示bangumi评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('bangumicomment:bangumicomment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return success(bangumiCommentService.getById(id));
    }

    /**
     * 新增展示bangumi评论
     */
    @PreAuthorize("@ss.hasPermi('bangumicomment:bangumicomment:add')")
    @Log(title = "展示bangumi评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BangumiComment bangumiComment)
    {
        return toAjax(bangumiCommentService.save(bangumiComment));
    }

    /**
     * 修改展示bangumi评论
     */
    @PreAuthorize("@ss.hasPermi('bangumicomment:bangumicomment:edit')")
    @Log(title = "展示bangumi评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BangumiComment bangumiComment)
    {
        return toAjax(bangumiCommentService.updateById(bangumiComment));
    }

    /**
     * 删除展示bangumi评论
     */
    @PreAuthorize("@ss.hasPermi('bangumicomment:bangumicomment:remove')")
    @Log(title = "展示bangumi评论", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable List<Long> ids)
    {
        return toAjax(bangumiCommentService.removeByIds(ids));
    }
}
