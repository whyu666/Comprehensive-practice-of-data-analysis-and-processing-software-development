package com.ruoyi.DAPMS.mapper;

import java.util.List;

import com.ruoyi.DAPMS.domain.User;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Mapper;

/**
 * user页面Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-24
 */
@Mapper
public interface UserMapper {
    /**
     * 查询user页面
     *
     * @param id user页面主键
     * @return user页面
     */
    @DataSource(value = DataSourceType.SLAVE)
    public User selectUserById(String id);

    /**
     * 查询user页面列表
     *
     * @param user user页面
     * @return user页面集合
     */
    @DataSource(value = DataSourceType.SLAVE)
    public List<User> selectUserList(User user);

    /**
     * 新增user页面
     *
     * @param user user页面
     * @return 结果
     */
    @DataSource(value = DataSourceType.SLAVE)
    public int insertUser(User user);

    /**
     * 修改user页面
     *
     * @param user user页面
     * @return 结果
     */
    @DataSource(value = DataSourceType.SLAVE)
    public int updateUser(User user);

    /**
     * 删除user页面
     *
     * @param id user页面主键
     * @return 结果
     */
    @DataSource(value = DataSourceType.SLAVE)
    public int deleteUserById(String id);

    /**
     * 批量删除user页面
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    @DataSource(value = DataSourceType.SLAVE)
    public int deleteUserByIds(String[] ids);
    @DataSource(value = DataSourceType.SLAVE)
    public List<String> queryTagsList(String userid);
}
