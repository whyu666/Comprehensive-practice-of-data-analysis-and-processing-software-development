package com.ruoyi.DAPMS.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.DAPMS.domain.Subject;
import com.ruoyi.DAPMS.domain.Tags;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 展示界面Mapper接口
 *
 * @author xyq
 * @date 2023-11-21
 */
@Repository
@DataSource(value = DataSourceType.SLAVE)
public interface SubjectMapper extends BaseMapper<Subject> {
    List<Subject> list(Subject subject);
    List<Tags> list_tag(
            @Param("subjects") List<Subject> subjects
    );
}
