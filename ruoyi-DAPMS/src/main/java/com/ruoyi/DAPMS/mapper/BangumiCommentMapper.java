package com.ruoyi.DAPMS.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.ruoyi.DAPMS.domain.BangumiComment;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 展示bangumi评论Mapper接口
 *
 * @author 吴春梅、任容
 * @date 2023-11-17
 */

@Repository
public interface BangumiCommentMapper extends BaseMapper<BangumiComment> {
    @DataSource(value = DataSourceType.SLAVE)
    List<BangumiComment> listlist(BangumiComment bangumiComment);
    @DataSource(value = DataSourceType.SLAVE)
    List<BangumiComment> listlist2(BangumiComment bangumiComment);
    @DataSource(value = DataSourceType.SLAVE)
    List<BangumiComment> getbangumiall(BangumiComment bangumiComment);
}
