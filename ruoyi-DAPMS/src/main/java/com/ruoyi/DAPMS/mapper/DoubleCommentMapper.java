package com.ruoyi.DAPMS.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.DAPMS.domain.DoubleComment;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;

import java.util.List;

/**
 * bangumi+doubanMapper接口
 *
 * @author 任容、吴春梅
 * @date 2023-11-22
 */
public interface DoubleCommentMapper extends BaseMapper<DoubleComment> {

//    所有List都使用从数据库
    @DataSource(value = DataSourceType.SLAVE)
    List<DoubleComment> bangumilist(DoubleComment doubleComment);

    @DataSource(value = DataSourceType.SLAVE)
    List<DoubleComment> listDouban(DoubleComment doubleComment);

    @DataSource(value = DataSourceType.SLAVE)
    List<DoubleComment> list2(DoubleComment doubleComment);

    @DataSource(value = DataSourceType.SLAVE)
    List<DoubleComment> listDouban2(DoubleComment doubleComment);

    @DataSource(value = DataSourceType.SLAVE)
    List<DoubleComment> getbangumiall(DoubleComment doubleComment);


}
