package com.ruoyi.DAPMS.mapper;

//import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.DAPMS.domain.DiagramCache;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
@DataSource(value = DataSourceType.SLAVE)
public interface DiagramCacheMapper{
//public interface DiagramCacheMapper extends BaseMapper<DiagramCache> {
    DiagramCache safeSelect(
            @Param("subject_id") String subjectId,
            @Param("min_score") int minScore,
            @Param("max_score") int maxScore
    );

    int safeInsert(
            @Param("cache") DiagramCache diagramCache
    );

    int safeUpdate(
            @Param("cache") DiagramCache diagramCache
    );
}
