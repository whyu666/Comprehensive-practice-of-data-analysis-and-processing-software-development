package com.ruoyi.DAPMS.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.DAPMS.domain.Comment;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@DataSource(value = DataSourceType.SLAVE)
public interface CommentMapper extends BaseMapper<Comment> {
    List<String> listCommentById(
            @Param("id") String id,
            @Param("min_score") int minScore,
            @Param("max_score") int maxScore
    );
}
