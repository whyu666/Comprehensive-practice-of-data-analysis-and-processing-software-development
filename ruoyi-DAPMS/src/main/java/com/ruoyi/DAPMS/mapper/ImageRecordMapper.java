package com.ruoyi.DAPMS.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.DAPMS.domain.ImageRecord;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.springframework.stereotype.Repository;

/**
 * 图片索引mapper接口
 *
 * @author tianq
 * Date: 2023-10-18
 */
@Repository
@DataSource(value = DataSourceType.SLAVE)
public interface ImageRecordMapper extends BaseMapper<ImageRecord> {

}
