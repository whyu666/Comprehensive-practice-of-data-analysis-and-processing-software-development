package com.ruoyi.DAPMS.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.DAPMS.domain.BangumiComment;

import java.util.List;

/**
 * 展示bangumi评论Service接口
 *
 * @author 吴春梅、任容
 * @date 2023-11-17
 */
public interface IBangumiCommentService extends IService<BangumiComment> {
    List<BangumiComment> listlist(BangumiComment bangumiComment);

    List<BangumiComment> listlist2(BangumiComment bangumiComment);

    List<BangumiComment> getbangumiall(BangumiComment bangumiComment);
}
