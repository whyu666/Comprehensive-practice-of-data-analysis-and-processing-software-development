package com.ruoyi.DAPMS.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.DAPMS.domain.Subject;
import com.ruoyi.DAPMS.domain.Tags;

import java.util.List;
/**
 * 展示界面Service接口
 *
 * @author xyq
 * @date 2023-11-21
 */
public interface ISubjectService extends IService<Subject> {
    List<Subject> list(Subject subject);
    List<Tags> list_tag(List<Subject> subjects);
}
