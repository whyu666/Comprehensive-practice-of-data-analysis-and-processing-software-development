package com.ruoyi.DAPMS.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.DAPMS.domain.BangumiComment;
import com.ruoyi.DAPMS.mapper.BangumiCommentMapper;
import com.ruoyi.DAPMS.service.IBangumiCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 展示bangumi评论Service业务层处理
 *
 * @author 吴春梅、任容
 * @date 2023-11-17
 */
@Service
public class BangumiCommentServiceImpl  extends ServiceImpl<BangumiCommentMapper, BangumiComment> implements IBangumiCommentService {
    @Autowired
    private BangumiCommentMapper bangumiCommentMapper;

    @Override
    public List<BangumiComment> listlist(BangumiComment bangumiComment) {
        return bangumiCommentMapper.listlist(bangumiComment);
    }

    @Override
    public List<BangumiComment> listlist2(BangumiComment bangumiComment) {
        return bangumiCommentMapper.listlist2(bangumiComment);
    }

    @Override
    public List<BangumiComment> getbangumiall(BangumiComment bangumiComment) {
        return bangumiCommentMapper.getbangumiall(bangumiComment);
    }
}
