package com.ruoyi.DAPMS.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.DAPMS.domain.DoubleComment;
import com.ruoyi.DAPMS.mapper.DoubleCommentMapper;
import com.ruoyi.DAPMS.service.IDoubleCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * bangumi+doubanService业务层处理
 *
 * @author 任容、吴春梅
 * @date 2023-11-22
 */
@Service
public class DoubleCommentServiceImpl extends ServiceImpl<DoubleCommentMapper, DoubleComment> implements IDoubleCommentService {

    @Autowired
    private DoubleCommentMapper doubleCommentMapper;

    @Override
    public List<DoubleComment> bangumilist(DoubleComment doubleComment) {
        return doubleCommentMapper.bangumilist(doubleComment);
    }

    @Override
    public List<DoubleComment> listDouban(DoubleComment doubleComment) {
        return doubleCommentMapper.listDouban(doubleComment);
    }

    @Override
    public List<DoubleComment> list2(DoubleComment doubleComment) {
        return doubleCommentMapper.list2(doubleComment);
    }

    @Override
    public List<DoubleComment> listDouban2(DoubleComment doubleComment) {
        return doubleCommentMapper.listDouban2(doubleComment);
    }

    @Override
    public List<DoubleComment> getbangumiall(DoubleComment doubleComment) {
        return doubleCommentMapper.getbangumiall(doubleComment);
    }


}
