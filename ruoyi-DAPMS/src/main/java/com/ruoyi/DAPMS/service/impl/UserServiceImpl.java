package com.ruoyi.DAPMS.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.DAPMS.mapper.UserMapper;
import com.ruoyi.DAPMS.domain.User;
import com.ruoyi.DAPMS.service.IUserService;

/**
 * user页面Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-11-24
 */
@Service
public class UserServiceImpl implements IUserService 
{
    @Autowired
    private UserMapper userMapper;

    /**
     * 查询user页面
     * 
     * @param id user页面主键
     * @return user页面
     */
    @Override
    public User selectUserById(String id)
    {
        return userMapper.selectUserById(id);
    }

    /**
     * 查询user页面列表
     * 
     * @param user user页面
     * @return user页面
     */
    @Override
    public List<User> selectUserList(User user)
    {
        List<User> ans = userMapper.selectUserList(user);
        for (int i = 0; i < ans.size(); i++) {
            ans.get(i).setTags(userMapper.queryTagsList(ans.get(i).getUserid()));
        }
        return ans;
    }

    /**
     * 新增user页面
     * 
     * @param user user页面
     * @return 结果
     */
    @Override
    public int insertUser(User user)
    {
        return userMapper.insertUser(user);
    }

    /**
     * 修改user页面
     * 
     * @param user user页面
     * @return 结果
     */
    @Override
    public int updateUser(User user)
    {
        return userMapper.updateUser(user);
    }

    /**
     * 批量删除user页面
     * 
     * @param ids 需要删除的user页面主键
     * @return 结果
     */
    @Override
    public int deleteUserByIds(String[] ids)
    {
        return userMapper.deleteUserByIds(ids);
    }

    /**
     * 删除user页面信息
     * 
     * @param id user页面主键
     * @return 结果
     */
    @Override
    public int deleteUserById(String id)
    {
        return userMapper.deleteUserById(id);
    }
}
