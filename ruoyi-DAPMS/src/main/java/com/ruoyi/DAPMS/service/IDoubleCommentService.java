package com.ruoyi.DAPMS.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.DAPMS.domain.DoubleComment;

import java.util.List;

/**
 * bangumi+doubanService接口
 *
 * @author 任容、吴春梅
 * @date 2023-11-22
 */
public interface IDoubleCommentService extends IService<DoubleComment> {
    List<DoubleComment> bangumilist(DoubleComment doubleComment);

    List<DoubleComment> listDouban(DoubleComment doubleComment);

    List<DoubleComment> list2(DoubleComment doubleComment);

    List<DoubleComment> listDouban2(DoubleComment doubleComment);

    List<DoubleComment> getbangumiall(DoubleComment doubleComment);


}
