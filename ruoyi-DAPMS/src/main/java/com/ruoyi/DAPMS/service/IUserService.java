package com.ruoyi.DAPMS.service;

import java.util.List;
import com.ruoyi.DAPMS.domain.User;

/**
 * user页面Service接口
 * 
 * @author ruoyi
 * @date 2023-11-24
 */
public interface IUserService 
{
    /**
     * 查询user页面
     * 
     * @param id user页面主键
     * @return user页面
     */
    public User selectUserById(String id);

    /**
     * 查询user页面列表
     * 
     * @param user user页面
     * @return user页面集合
     */
    public List<User> selectUserList(User user);

    /**
     * 新增user页面
     * 
     * @param user user页面
     * @return 结果
     */
    public int insertUser(User user);

    /**
     * 修改user页面
     * 
     * @param user user页面
     * @return 结果
     */
    public int updateUser(User user);

    /**
     * 批量删除user页面
     * 
     * @param ids 需要删除的user页面主键集合
     * @return 结果
     */
    public int deleteUserByIds(String[] ids);

    /**
     * 删除user页面信息
     * 
     * @param id user页面主键
     * @return 结果
     */
    public int deleteUserById(String id);
}
