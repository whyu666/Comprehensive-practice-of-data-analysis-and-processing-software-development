package com.ruoyi.DAPMS.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.DAPMS.domain.ImageRecord;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.util.List;

public interface IImageService extends IService<ImageRecord> {

    /**
     * @param image 要保存的图片
     * @return 所保存图片的索引记录信息
     * @apiNote 保存图片到文件
     * @author tianq
     * Date: 2023-10-18
     */
    ImageRecord storeImageBuf(BufferedImage image);

    /**
     * @param image 要保存的图片byte
     * @return 所保存图片的索引记录信息
     * @apiNote 保存图片到文件
     * @author tianq
     * Date: 2023-10-22
     */
    ImageRecord storeImageByte(byte[] image);

    /**
     * @param image 要保存的图片file
     * @return 所保存图片的索引记录信息
     * @apiNote 保存图片到文件
     * @author tianq
     * Date: 2023-10-22
     */
    ImageRecord storeImageFile(MultipartFile image);

    /**
     * @param image 要保存的图片
     * @param alias 图片别名，不允许重复
     * @return 所保存图片的索引记录信息
     * @apiNote 连带别名保存Buf图片到文件
     * @author tianq
     * Date: 2023-10-18
     */
    ImageRecord storeBufWithAlias(BufferedImage image, String alias);

    /**
     * @param image 要保存的图片
     * @param alias 图片别名，不允许重复
     * @return 所保存图片的索引记录信息
     * @apiNote 连带别名保存byte图片到文件
     * @author tianq
     * Date: 2023-10-18
     */
    ImageRecord storeByteWithAlias(byte[] image, String alias);

    /**
     * @param path 图片存储的路径与文件名
     * @return 所获取图片的数据
     * @apiNote 按路径获取图片数据
     * @author tianq
     * Date: 2023-10-19
     */
    BufferedImage getImageByPath(String path);

    /**
     * @param imageId 获取图片的ID
     * @return 所获取图片的数据
     * @apiNote 按ID获取图片数据
     * @author tianq
     * Date: 2023-10-18
     */
    BufferedImage getImageById(Long imageId);

    /**
     * @param alias 获取图片的别名字符串
     * @return 所获取图片的数据
     * @apiNote 按别名获取图片数据
     * @author tianq
     * Date: 2023-10-18
     */
    BufferedImage getImageByAlias(String alias);

    // ----------------
    // 下面的方法都是扩展功能，选做

    /**
     * @param imageId 要移动的图片的ID
     * @param path    目标路径&文件名
     * @return 移动后的图片索引
     * @apiNote 移动图片存储位置
     * @author tianq
     * Date: 2023-10-18
     * @deprecated 新存储架构下不允许低级文件操作
     */
    ImageRecord moveImage(Long imageId, String path);

    /**
     * @param imageId 被复制图片的ID
     * @return 新图片的索引，含有新文件名和别名
     * @apiNote 复制图片文件
     * @author tianq
     * Date: 2023-10-18
     * @deprecated 新存储架构下不允许低级文件操作
     */
    ImageRecord copyImage(Long imageId);

    /**
     * @param imageId 待删除图片的ID
     * @return 删除后的图片索引对象
     * @apiNote 删除图片文件
     * @author tianq
     * Date: 2023-10-18
     * @deprecated 新存储架构下不允许低级文件操作
     */
    ImageRecord deleteImage(Long imageId);

    /**
     * @param validityPeriod 有效期，单位天
     * @return 被删除图片的ID列表
     * @apiNote 删除所有到期且无引用的图片
     * @author tianq
     * Date: 2023-10-18
     * @deprecated 新存储架构下不允许低级文件操作
     */
    List<Long> autoRemove(Long validityPeriod);


}
