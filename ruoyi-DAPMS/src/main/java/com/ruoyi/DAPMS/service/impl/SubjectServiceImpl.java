package com.ruoyi.DAPMS.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.DAPMS.domain.Subject;
import com.ruoyi.DAPMS.domain.Tags;
import com.ruoyi.DAPMS.mapper.SubjectMapper;
import com.ruoyi.DAPMS.service.ISubjectService;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 展示界面Service业务层处理
 *
 * @author xyq
 * @date 2023-11-21
 */
@Service
@DataSource(value = DataSourceType.SLAVE)
public class SubjectServiceImpl extends ServiceImpl<SubjectMapper, Subject> implements ISubjectService {
    @Autowired
    private SubjectMapper subjectMapper;

    @Override
    public List<Subject> list(Subject subject) {
        List<Subject> s_list = subjectMapper.list(subject);
        List<Tags> tags = subjectMapper.list_tag(s_list);
        int flag=0;
        for(int i=0;i<s_list.size();i++){
            for(int j=0;j<tags.size();j++){
                if(s_list.get(i).getId().equals(tags.get(j).getId())){
                    s_list.get(i).getTags().add(tags.get(j).getTag());
                    flag=1;
                }
            }
            if(flag==1){
                s_list.get(i).setTagsum(s_list.get(i).getTags().size());
                flag=0;
            }
            else {
                s_list.get(i).setTagsum(1);
                s_list.get(i).getTags().add("无标签");
            }
        }
        return s_list;
    }

    @Override
    public List<Tags> list_tag(List<Subject> subjects){
        return subjectMapper.list_tag(subjects);
    };

    @Override
    @DataSource(value = DataSourceType.SLAVE)
    public List<Subject> list(Wrapper<Subject> queryWrapper) {
        return super.list(queryWrapper);
    }
}
