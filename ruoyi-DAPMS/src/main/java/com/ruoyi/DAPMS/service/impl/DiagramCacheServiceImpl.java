package com.ruoyi.DAPMS.service.impl;

import com.alibaba.fastjson2.JSONArray;
import com.kennycason.kumo.CollisionMode;
import com.kennycason.kumo.WordCloud;
import com.kennycason.kumo.WordFrequency;
import com.kennycason.kumo.font.KumoFont;
import com.kennycason.kumo.font.scale.SqrtFontScalar;
import com.kennycason.kumo.image.AngleGenerator;
import com.kennycason.kumo.nlp.FrequencyAnalyzer;
import com.kennycason.kumo.nlp.tokenizers.ChineseWordTokenizer;
import com.kennycason.kumo.palette.ColorPalette;
import com.ruoyi.DAPMS.domain.DiagramCache;
import com.ruoyi.DAPMS.domain.ImageRecord;
import com.ruoyi.DAPMS.mapper.CommentMapper;
import com.ruoyi.DAPMS.mapper.DiagramCacheMapper;
import com.ruoyi.DAPMS.service.IDiagramCacheService;
import com.ruoyi.DAPMS.service.IImageService;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class DiagramCacheServiceImpl implements IDiagramCacheService {
//public class DiagramCacheServiceImpl extends ServiceImpl<DiagramCacheMapper, DiagramCache> implements IDiagramCacheService {

    IImageService imageService;
    CommentMapper commentMapper;
    DiagramCacheMapper diagramCacheMapper;

    @Autowired
    public void setImageService(IImageService imageService) {
        this.imageService = imageService;
    }

    @Autowired
    public void setCommentMapper(CommentMapper commentMapper) {
        this.commentMapper = commentMapper;
    }

    @Autowired
    public void setDiagramCacheMapper(DiagramCacheMapper diagramCacheMapper) {
        this.diagramCacheMapper = diagramCacheMapper;
    }

    @Override
    public BufferedImage getDiagram(String subjectId, int minScore, int maxScore) {

        // 查找缓存
        DiagramCache cache = diagramCacheMapper.safeSelect(subjectId, minScore, maxScore);
//        DiagramCache cache = getDiagramCache(subjectId, minScore, maxScore);

        // 无缓存，现场生成并缓存
        if (Objects.isNull(cache)) {
            return makeDiagramCache(subjectId, minScore, maxScore);
        }

        // 有图片缓存
        Long imageId = cache.getImageId();
        if (!Objects.isNull(imageId)) {
            try {
                BufferedImage image = imageService.getImageById(imageId);
                if(!Objects.isNull(image)) return image;
            } catch (Exception e) {
//                ImageRecord record = imageService.getById(imageId);
//                record.setDeleted(1L);
//                imageService.updateById(record);
                System.out.println(
                        "[wanning]DiagramCacheServiceImpl:getDiagram" +
                                "(subjectId=" + subjectId + ", minScore=" + minScore + ", maxScore=" + maxScore + "):" +
                                "get image by id(" + imageId + ") failed, does that file exist? " +
                                "fallback: image cache ignored, continuing"
                );
            }
        }

        // 有词频缓存
        JSONArray array = JSONArray.parse(cache.getWordFreq());
        List<WordFrequency> wordFrequencies = array.toJavaList(WordFrequency.class);
        BufferedImage image = makeImageWithFreq(wordFrequencies);

        // 缓存图片
        String cur_date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String alias = "wordcloud_id" + subjectId + "_min" + minScore + "_max" + maxScore + "_time" + cur_date;
        ImageRecord record = imageService.storeBufWithAlias(image, alias);
        cache.setImageId(record.getImageId());
        diagramCacheMapper.safeUpdate(cache);

        return image;
    }

    @Override
    public List<WordFrequency> getFrequencies(String subjectId, int minScore, int maxScore) {

        // 查找缓存
        DiagramCache cache = diagramCacheMapper.safeSelect(subjectId, minScore, maxScore);
//        DiagramCache cache = getDiagramCache(subjectId, minScore, maxScore);

        // 无缓存，现场生成并缓存
        if (Objects.isNull(cache)) {

            List<String> comments = getComments(subjectId, minScore, maxScore);
            List<WordFrequency> wordFrequencies = makeFreqWithComments(comments);
            String frequencies = new JSONArray(wordFrequencies).toString();

            cache = new DiagramCache(subjectId, minScore, maxScore, null, frequencies);
            diagramCacheMapper.safeInsert(cache);
        }

        JSONArray array = JSONArray.parse(cache.getWordFreq());

        return array.toJavaList(WordFrequency.class);
    }

    @Override
    public List<String> getComments(String subjectId, int minScore, int maxScore) {
        return commentMapper.listCommentById(subjectId, minScore, maxScore);
    }

//    private DiagramCache getDiagramCache(String subjectId, int minScore, int maxScore) {
//
//        // 查找缓存
//        QueryWrapper<DiagramCache> wrapper = new QueryWrapper<>();
//        wrapper.eq("subject_id",subjectId);
//        wrapper.eq("min_score",minScore);
//        wrapper.eq("max_score",maxScore);
//        wrapper.orderByDesc("create_time");
//
//        List<DiagramCache> caches = list(wrapper);
//
//        // 无缓存
//        if(caches.isEmpty()){
//            return null;
//        }
//
//        // 允许更新缓存版本，此功能尚不稳定
//        if (caches.size()>1) {
//            String msg =
//                    "[debug]DiagramCacheServiceImpl::getDiagram" +
//                            "(subjectId="+subjectId+", minScore="+minScore+", maxScore="+maxScore+"): " +
//                            "multiple caches of requested diagram found, using newest one";
//            System.out.println(msg);
//        }
//        return caches.get(0);
//    }

    @Override
    public BufferedImage makeDiagramCache(String subjectId, int minScore, int maxScore) {

        List<String> comments = getComments(subjectId, minScore, maxScore);
        if (Objects.isNull(comments) || comments.isEmpty()) return null;
        List<WordFrequency> wordFrequencies = makeFreqWithComments(comments);
        String frequencies = new JSONArray(wordFrequencies).toString();
        BufferedImage image = makeImageWithFreq(wordFrequencies);

        // 缓存图片
        String cur_date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String alias = "wordcloud_id" + subjectId + "_min" + minScore + "_max" + maxScore + "_time" + cur_date;
        ImageRecord record = imageService.storeBufWithAlias(image, alias);

        // 缓存词频
        DiagramCache cache = new DiagramCache(
                subjectId,
                minScore,
                maxScore,
                record.getImageId(),
                frequencies
        );

        diagramCacheMapper.safeInsert(cache);
//        this.save(cache);

        return image;
    }

    private BufferedImage makeImageWithFreq(List<WordFrequency> wordFrequencies) {

        final Dimension dimension = new Dimension(800, 600);
        final WordCloud wordCloud = new WordCloud(dimension, CollisionMode.PIXEL_PERFECT);
        wordCloud.setPadding(5);
        wordCloud.setBackgroundColor(Color.WHITE);
//        wordCloud.setBackground(new CircleBackground(300));
        wordCloud.setColorPalette(new ColorPalette(new Color(0xD5CFFA), new Color(0xBBB1FA), new Color(0x9A8CF5), new Color(0x806EF5)));
//        wordCloud.setColorPalette(new LinearGradientColorPalette(Color.RED, Color.BLUE, Color.GREEN, 30, 30));
//        wordCloud.setFontScalar(new LinearFontScalar(12,45));
        wordCloud.setFontScalar(new SqrtFontScalar(12, 45));
        wordCloud.setKumoFont(new KumoFont(new Font("Microsoft YaHei", Font.PLAIN, 16)));
        wordCloud.setAngleGenerator(new AngleGenerator(0));
        wordCloud.build(wordFrequencies);

        return wordCloud.getBufferedImage();
    }

    private List<WordFrequency> makeFreqWithComments(List<String> comments) {

        final FrequencyAnalyzer frequencyAnalyzer = new FrequencyAnalyzer();
        frequencyAnalyzer.setWordFrequenciesToReturn(200);
        frequencyAnalyzer.setMinWordLength(2);
        frequencyAnalyzer.setWordTokenizer(new ChineseWordTokenizer());

        final List<WordFrequency> wordFrequencies;
        wordFrequencies = frequencyAnalyzer.load(comments);
        return wordFrequencies;
    }
}
