package com.ruoyi.DAPMS.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.DAPMS.domain.ImageRecord;
import com.ruoyi.DAPMS.mapper.ImageRecordMapper;
import com.ruoyi.DAPMS.service.IImageService;
import com.ruoyi.common.annotation.DataSource;
import com.ruoyi.common.enums.DataSourceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@DataSource(value = DataSourceType.SLAVE)
public class ImageServiceImpl extends ServiceImpl<ImageRecordMapper, ImageRecord> implements IImageService {

    private ImageRecordMapper imageRecordMapper;

    // 图片保存根路径
    private String basePath = "classpath:/saved-images";

    // 图片保存格式
    private String saveFormat = "jpg";

    // 读取错误则删除
    private boolean deleteOnException = false;


    @Autowired
    public void setImageRecordMapper(ImageRecordMapper imageRecordMapper) {
        this.imageRecordMapper = imageRecordMapper;
    }

    @Value(value = "${tianq.image.base-path}")
    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    @Value(value = "${tianq.image.save-format}")
    public void setSaveFormat(String saveFormat) {
        this.saveFormat = saveFormat;
    }

    @Value(value = "${tianq.image.delete-on-exception}")
    public void setDeleteOnException(boolean deleteOnException) {
        this.deleteOnException = deleteOnException;
    }

    /**
     * @author tianq
     * Date: 2023-10-22
     * @implNote 图片文件保存回调
     */
    private interface SaverCallback {
        //保存图片到指定文件
        void saveTo(File saveFile);
    }

    /**
     * @param image 要保存的图片
     * @param file  保存到的文件
     * @implNote 保存BufferedImage图片时使用的回调函数
     * @author tianq
     * Date: 2023-10-22
     */
    private void saveImageBuffered(BufferedImage image, File file) {
        try {
            boolean bWriteFlag = ImageIO.write(image, saveFormat, file);
            if (bWriteFlag) {
                System.out.println("[debug]ImageService::saveImageBuffered:success");
            } else {
                System.out.println("[error]ImageService::saveImageBuffered:fucked up");
                throw new RuntimeException("[error]ImageService::saveImageBuffered:fucked up");
            }
        } catch (IOException e) {
            System.out.println("[error]ImageService::saveImageBuffered:damn it");
            throw new RuntimeException(e);
        }
    }

    /**
     * @param imageBytes 要保存的图片
     * @param file       保存到的文件
     * @implNote 保存byte[]图片时使用的回调函数
     * @author tianq
     * Date: 2023-10-22
     */
    private void saveImageByte(byte[] imageBytes, File file) {
        // 保存图片到本地文件系统
        // 此处只完成文件转码
        ByteArrayInputStream stream = new ByteArrayInputStream(imageBytes);
        try {
            BufferedImage bufferedImage = ImageIO.read(stream);
            saveImageBuffered(bufferedImage, file);
        } catch (IOException e) {
            System.out.println("[error]ImageService::saveImageByte:fucked up here");
            throw new RuntimeException(e);
        }

    }

    private ImageRecord storeImage(SaverCallback saver) {

        //保存文件的相对目录
        String relFolder = new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
        //保存文件的绝对目录
        String absFolder = basePath + relFolder;

        // 按时间递归创建文件夹
        File folder = new File(absFolder);
        if (!folder.isDirectory()) {
            if (!folder.mkdirs()) {
                System.out.println(
                        "[error]ImageService::storeImage(image): " +
                                "Cannot create folder: " + absFolder +
                                ", giving up"
                );
                return null;
            }
        }

        //默认使用uuid作为别名
        String defaultAlias = UUID.randomUUID().toString();
        // 文件名UUID
        String fileName = defaultAlias + "." + saveFormat;
        // 图片索引中保存的相对路径
        String relPath = relFolder + fileName;
        // 文件绝对路径
        String absPath = absFolder + fileName;

        System.out.println(
                "[debug]ImageService::storeImage(image): " +
                        "Saving image to: " + absPath
        );

        // 创建图片文件
        File imageFile = new File(folder, fileName);

        // 保存图片到文件
        // 通过保存方法回调，实现多种传入方案支持
        saver.saveTo(imageFile);

        // 创建图片索引对象
        ImageRecord imageRecord = new ImageRecord();
        imageRecord.setStoragePath(relPath);
        imageRecord.setImageAlias(defaultAlias);
        imageRecord.setCreateTime(new Date());
//        imageRecord.setCreateTime(new Timestamp(new java.util.Date().getTime()));

        // 保存到数据库
        imageRecordMapper.insert(imageRecord);

        System.out.println(
                "[debug]ImageService::storeImage(image)" +
                        "Image record inserted: " + imageRecord
        );

        return imageRecord;
    }

    /**
     * @param image 要保存的图片BufferedImage
     * @return 所保存图片的索引记录信息
     * @apiNote 保存图片到文件
     * @author tianq
     * Date: 2023-10-22
     */
    @Override
    public ImageRecord storeImageBuf(BufferedImage image) {
        return storeImage(saveFile -> saveImageBuffered(image, saveFile));
    }

    /**
     * @param image 要保存的图片Byte
     * @return 所保存图片的索引记录信息
     * @apiNote 保存图片到文件
     * @author tianq
     * Date: 2023-10-22
     */
    @Override
    public ImageRecord storeImageByte(byte[] image) {
        return storeImage(saveFile -> saveImageByte(image, saveFile));
    }

    /**
     * @param image 要保存的图片file
     * @return 所保存图片的索引记录信息
     * @apiNote 保存图片到文件
     * @author tianq
     * Date: 2023-10-22
     */
    @Override
    public ImageRecord storeImageFile(MultipartFile image) {
        return storeImage(saveFile -> {
            try {
                image.transferTo(saveFile);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    /**
     * @param saver 保存图片所用的回调
     * @param alias 图片别名，不允许重复
     * @return 所保存图片的索引记录信息
     * @apiNote 连带别名保存图片到文件
     * @author tianq
     * Date: 2023-10-18
     */
    private ImageRecord storeWithAlias(SaverCallback saver, String alias) {

        // 尝试保存
        ImageRecord imageRecord = storeImage(saver);
        // 保存异常处理
        if (imageRecord == null) {
            return null;
        }

        // 检查别名冲突
        if (!"".equals(alias)) {
            QueryWrapper<ImageRecord> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("image_alias", alias);
            Integer aliasConflict = imageRecordMapper.selectCount(queryWrapper);
            if (aliasConflict != 0) {
                System.out.println(
                        "[warning]ImageService::storeWithAlias(image, alias=" + alias + "):" +
                                "Alias conflict found! " +
                                "Keeping default alias: " + imageRecord.getImageAlias()
                );
                return imageRecord;
            }
        }

        // 无冲突, 应用指定的别名
        imageRecord.setImageAlias(alias);
        imageRecordMapper.updateById(imageRecord);

        System.out.println(
                "[debug]ImageService::storeWithAlias(image, alias=" + alias + "):" +
                        "Succeeded, returning: " + imageRecord
        );

        return imageRecord;
    }

    /**
     * @param image 要保存的图片
     * @param alias 图片别名，不允许重复
     * @return 所保存图片的索引记录信息
     * @apiNote 连带别名保存Buf图片到文件
     * @author tianq
     * Date: 2023-10-18
     */
    @Override
    public ImageRecord storeBufWithAlias(BufferedImage image, String alias) {
        return storeWithAlias(saveFile -> saveImageBuffered(image, saveFile), alias);
    }

    /**
     * @param image 要保存的图片
     * @param alias 图片别名，不允许重复
     * @return 所保存图片的索引记录信息
     * @apiNote 连带别名保存byte图片到文件
     * @author tianq
     * Date: 2023-10-18
     */
    @Override
    public ImageRecord storeByteWithAlias(byte[] image, String alias) {
        return storeWithAlias(saveFile -> saveImageByte(image, saveFile), alias);
    }

    /**
     * @param path 图片存储的路径与文件名
     * @return 所获取图片的数据
     * @apiNote 按路径获取图片数据
     * @author tianq
     * Date: 2023-10-19
     */
    @Override
    public BufferedImage getImageByPath(String path) {

        System.out.println(
                "[debug]ImageService::getImageByPath(path=" + path + ")" +
                        "Reading image from: " + path
        );

        // 读取文件并返回
        File file = new File(basePath + path);
        try {
            return ImageIO.read(file);
        } catch (IOException e) {
            System.out.println(
                    "[error]ImageService::getImageByPath(path=" + path + ")" +
                            "IOException when reading: " + path
            );
            throw new RuntimeException(e);
        }
    }

    /**
     * @param imageId 获取图片的ID
     * @return 所获取图片的数据
     * @apiNote 按ID获取图片数据
     * @author tianq
     * Date: 2023-10-18
     */
    @Override
    public BufferedImage getImageById(Long imageId) {

        // 从数据库中获取存储信息
        ImageRecord imageRecord = imageRecordMapper.selectById(imageId);
        // 获取失败处理，返回null
        if (Objects.isNull(imageRecord)) {
            System.out.println(
                    "[warning]ImageService::getImageById(imageId=" + imageId + ")" +
                            "Can not fetch image record, check if exist or deleted. " +
                            "Returning null here"
            );
            return null;
        }

        try {
            return getImageByPath(imageRecord.getStoragePath());
        } catch (Exception e) {

            if (!deleteOnException) {
                System.out.println(
                        "[warning]ImageService::getImageById(imageId=" + imageId + ")" +
                                "Image record found, but cannot read from path(" + imageRecord.getStoragePath() + ") " +
                                "not updating image record, returning null here"
                );
                return null;
            }

//            imageRecord.setDeleted(1L);
//            imageRecordMapper.updateById(imageRecord);
//            imageRecordMapper.deleteById(imageId);
            this.removeById(imageRecord.getImageId());
            System.out.println(
                    "[warning]ImageService::getImageById(imageId=" + imageId + ")" +
                            "Image record found, but cannot read from path(" + imageRecord.getStoragePath() + ") " +
                            "Updating record as deleted, returning null here"
            );
            return null;
        }
    }

    /**
     * @param alias 获取图片的别名字符串
     * @return 所获取图片的数据
     * @apiNote 按别名获取图片数据
     * @author tianq
     * Date: 2023-10-18
     */
    @Override
    public BufferedImage getImageByAlias(String alias) {

        QueryWrapper<ImageRecord> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("image_alias", alias);
        List<ImageRecord> imageRecordList = imageRecordMapper.selectList(queryWrapper);

        if (imageRecordList.size() != 1) {
            System.out.println(
                    "[warning]ImageService::getImageByAlias(alias=" + alias + ")" +
                            "Expected selecting 1 entity, but " + imageRecordList.size() + " were found!" +
                            "Returning null here"
            );
            return null;
        }

        ImageRecord imageRecord = imageRecordList.get(0);

        try {
            return getImageByPath(imageRecord.getStoragePath());
        } catch (Exception e) {

            if (!deleteOnException) {
                System.out.println(
                        "[warning]ImageService::getImageByAlias(alias=" + alias + ")" +
                                "Image record found, but cannot read from path(" + imageRecord.getStoragePath() + ") " +
                                "not updating image record, returning null here"
                );
                return null;
            }

//            imageRecord.setDeleted(1L);
//            imageRecordMapper.updateById(imageRecord);
            this.removeById(imageRecord.getImageId());
            System.out.println(
                    "[warning]ImageService::getImageByAlias(alias=" + alias + ")" +
                            "Image record found, but cannot read from path(" + imageRecord.getStoragePath() + ") " +
                            "Updating record as deleted, returning null here"
            );
            return null;
        }

    }

    /**
     * @param imageId 要移动的图片的ID
     * @param path    目标路径&文件名
     * @return 移动后的图片索引，失败时为null
     * @apiNote 移动图片存储位置
     * @author tianq
     * Date: 2023-10-18
     * @deprecated 新存储架构下不允许低级文件操作
     */
    @Override
    public ImageRecord moveImage(Long imageId, String path) {

        // 从数据库中获取存储信息
        ImageRecord imageRecord = imageRecordMapper.selectById(imageId);
        // 获取失败处理，返回null
        if (Objects.isNull(imageRecord)) {
            System.out.println(
                    "[warning]ImageService::moveImage(imageId=" + imageId + ", path=" + path + ")" +
                            "Can not fetch image record, check if exist or deleted. " +
                            "Returning null here"
            );
            return null;
        }

        // 原路径
        String oldPath = imageRecord.getStoragePath();
        // 新路径
        String newPath;
        // 新目录，即移动到的目录
        String newFolder;

        // 判断目标路径的格式
        if (path.lastIndexOf(File.separatorChar) == path.length() - 1) {
            // 输入新路径为目录，则默认使用原先的文件名
            newPath = path + oldPath.substring(oldPath.lastIndexOf(File.separatorChar));
            newFolder = path;
        } else {
            // 输入新路径为路径，则从路径中取目录
            newPath = path;
            newFolder = path.substring(0, path.lastIndexOf(File.separatorChar));
        }

        System.out.println(
                "[debug]ImageService::moveImage(imageId=" + imageId + ", path=" + path + ")" +
                        "Moving from: " + oldPath + "to: " + newPath +
                        "with folder: " + newFolder
        );

        // 递归创建目录
        File folder = new File(newFolder);
        if (!folder.isDirectory()) {
            if (!folder.mkdirs()) {
                System.out.println(
                        "[error]ImageService::moveImage(imageId=" + imageId + ", path=" + path + ")" +
                                "Cannot create folder: " + newFolder +
                                ", giving up"
                );
                return null;
            }
        }


        // 移动文件
        File oldFile = new File(basePath + oldPath);
        File newFile = new File(basePath + newPath);

        try {
            Files.move(oldFile.toPath(), newFile.toPath());
            imageRecord.setStoragePath(newPath);
            imageRecordMapper.updateById(imageRecord);
            // TODO: 这里可以添加删除空目录的逻辑

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return imageRecord;
    }

    /**
     * @param imageId 被复制图片的ID
     * @return 新图片的索引，含有新文件名和别名
     * @apiNote 复制图片文件
     * @author tianq
     * Date: 2023-10-18
     * @deprecated 新存储架构下不允许低级文件操作
     */
    @Override
    public ImageRecord copyImage(Long imageId) {
        BufferedImage image = getImageById(imageId);
        return storeImageBuf(image);
    }

    /**
     * @param imageId 待删除图片的ID
     * @return 删除后的图片索引对象
     * @apiNote 删除图片文件
     * @author tianq
     * Date: 2023-10-18
     * @deprecated 新存储架构下不允许低级文件操作
     */
    @Override
    public ImageRecord deleteImage(Long imageId) {

        // 从数据库中获取存储信息
        ImageRecord imageRecord = imageRecordMapper.selectById(imageId);
        // 获取失败处理，返回null
        if (Objects.isNull(imageRecord)) {
            System.out.println(
                    "[warning]ImageService::deleteImage(imageId=" + imageId + ")" +
                            "Can not fetch image record, check if exist or deleted. " +
                            "Returning null here"
            );
            return null;
        }

        //尝试删除
        File file = new File(basePath + imageRecord.getStoragePath());
        boolean deleted = file.delete();
        if (deleted) {
            imageRecord.setDeleted(1L);
            imageRecordMapper.updateById(imageRecord);
        }

        return imageRecord;
    }

    /**
     * @param validityPeriod 有效期，单位天
     * @return 被删除图片的ID列表
     * @apiNote 删除所有到期且无引用的图片
     * @author tianq
     * Date: 2023-10-18
     * @deprecated 新存储架构下不允许低级文件操作
     */
    @Override
    public List<Long> autoRemove(Long validityPeriod) {
        return null;
    }
}
