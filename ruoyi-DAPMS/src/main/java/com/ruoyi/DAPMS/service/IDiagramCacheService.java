package com.ruoyi.DAPMS.service;

import com.kennycason.kumo.WordFrequency;

//import com.baomidou.mybatisplus.extension.service.IService;
//import com.ruoyi.DAPMS.domain.DiagramCache;

import java.awt.image.BufferedImage;
import java.util.List;

public interface IDiagramCacheService{
//public interface IDiagramCacheService extends IService<DiagramCache>{
    /**
     * @apiNote 获取词云图（使用缓存）
     * @param subjectId 动画ID
     * @param minScore  最低分
     * @param maxScore  最高分
     * @return  词云图（使用缓存）
     */
    BufferedImage getDiagram(String subjectId, int minScore, int maxScore);

    /**
     * @apiNote 获取评论词频（来自缓存）
     * @param subjectId 动画ID
     * @param minScore  最低分
     * @param maxScore  最高分
     * @return  满足查询条件的评论词频列表
     */
    List<WordFrequency> getFrequencies(String subjectId, int minScore, int maxScore);

    /**
     * @apiNote 获取所有评论
     * @param subjectId 动画ID
     * @param minScore  最低分
     * @param maxScore  最高分
     * @return  满足查询条件的评论列表
     */
    List<String> getComments(String subjectId, int minScore, int maxScore);

    /**
     * @apiNote 获取词云图（不使用缓存，总是生成一张新的）
     * @param subjectId 动画ID
     * @param minScore  最低分
     * @param maxScore  最高分
     * @return  新生成的词云图
     */
    BufferedImage makeDiagramCache(String subjectId, int minScore, int maxScore);

}
