package com.ruoyi.DAPMS.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 展示界面对象 subject
 *
 * @author xyq
 * @date 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Tags {
    /** 作品ID */
    private String id;
    /** 作品tag */
    private String tag;
}
