package com.ruoyi.DAPMS.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 展示界面对象 subject
 *
 * @author xyq
 * @date 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("subject")
public class Subject {
    /** 排名 */
    @TableId(type = IdType.AUTO)
    private String rk;

    /** 作品ID */
    private String id;

    /** 作品中文名 */
    private String chineseName;

    /** 作品日文名 */
    private String japaneseName;

    /** 话数 */
    private String number;

    /** 放送开始时间 */
    private String startTime;

    /** 评分 */
    private String score;

    /** 评分数 */
    private String scoreNumber;

    /** 是否为限制级作品 */
    private String flag;

    /** 豆瓣作品ID */
    private String doubanId;

    /** 豆瓣作品名 */
    private String doubanName;

    /** 豆瓣评分 */
    private String doubanScore;

    /** 豆瓣评分数 */
    private String doubanScoreNumber;

    /**分析*/
    private String analyse;

    /** 作品tag */
    @TableField(exist = false)
    private List<String> tags = new ArrayList<>();

    /** 作品tag总数 */
    @TableField(exist = false)
    private Integer tagsum=0;

}
