package com.ruoyi.DAPMS.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
/**
 * bangumi+douban对象 doublecomment
 *
 * @author 任容、吴春梅
 * @date 2023-11-22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("comment")
public class DoubleComment implements Serializable {
    @TableField(exist = false)
    private String quantity;

    @TableField(exist = false)
    private String quantity2;

    @TableField(exist = false)
    private String chname;
    @TableField(exist = false)
    private String bangumiallscore;
    @TableField(exist = false)
    private String bangumiallnum;
    @TableField(exist = false)
    private String doubanallscore;
    @TableField(exist = false)
    private String doubanallnum;


    /** 作品ID */
    private String id;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 用户ID */
    @Excel(name = "用户ID")
    private String userid;

    /** 评论时间 */
    @Excel(name = "评论时间")
    private String time;

    /** 评论内容 */
    @Excel(name = "评论内容")
    private String comment;

    /** 评分 */
    @Excel(name = "评分")
    private String score;

//    以下是豆瓣表相关

//    豆瓣ID
    @TableField(exist = false)
    private String douban_id;

//    豆瓣评论者用户名
    @TableField(exist = false)
    private String dbusername;

//    豆瓣评论时间
    @TableField(exist = false)
    private String dbtime;

//    豆瓣评论内容
    @TableField(exist = false)
    private String dbcomment;

//    豆瓣用户个人评分
    @TableField(exist = false)
    private String dbscore;


}
