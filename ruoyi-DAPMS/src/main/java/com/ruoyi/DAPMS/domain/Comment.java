package com.ruoyi.DAPMS.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "comment")
public class Comment {

    @TableId(type = IdType.INPUT)

    private String id;

    private String username;

    private String userid;

    @JsonFormat(pattern = "yyyy-MM-dd kk:mm")
    private Date time;

    private String comment;

    private String score;

}
