package com.ruoyi.DAPMS.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * user页面对象 user
 * 
 * @author ruoyi
 * @date 2023-11-24
 */
public class User extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 标号 */
    private String id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private String userid;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 加入时间 */
    @Excel(name = "加入时间")
    private String joinTime;

    /** 收藏数 */
    @Excel(name = "收藏数")
    private String favoriteNumber;

    /** 完成数 */
    @Excel(name = "完成数")
    private String watchedNumber;

    /** 完成率 */
    @Excel(name = "完成率")
    private String completePercentage;

    /** 平均分 */
    @Excel(name = "平均分")
    private String averageScore;

    /** 标准差 */
    @Excel(name = "标准差")
    private String standardDeviation;

    /** 评分数 */
    @Excel(name = "评分数")
    private String scoreNumber;

    /** 分析结果 */
    @Excel(name = "分析结果")
    private String analyse;

    @Excel(name = "tags")
    private List<String> tags;

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserid(String userid) 
    {
        this.userid = userid;
    }

    public String getUserid() 
    {
        return userid;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setJoinTime(String joinTime) 
    {
        this.joinTime = joinTime;
    }

    public String getJoinTime() 
    {
        return joinTime;
    }
    public void setFavoriteNumber(String favoriteNumber) 
    {
        this.favoriteNumber = favoriteNumber;
    }

    public String getFavoriteNumber() 
    {
        return favoriteNumber;
    }
    public void setWatchedNumber(String watchedNumber) 
    {
        this.watchedNumber = watchedNumber;
    }

    public String getWatchedNumber() 
    {
        return watchedNumber;
    }
    public void setCompletePercentage(String completePercentage) 
    {
        this.completePercentage = completePercentage;
    }

    public String getCompletePercentage() 
    {
        return completePercentage;
    }
    public void setAverageScore(String averageScore) 
    {
        this.averageScore = averageScore;
    }

    public String getAverageScore() 
    {
        return averageScore;
    }
    public void setStandardDeviation(String standardDeviation) 
    {
        this.standardDeviation = standardDeviation;
    }

    public String getStandardDeviation() 
    {
        return standardDeviation;
    }
    public void setScoreNumber(String scoreNumber) 
    {
        this.scoreNumber = scoreNumber;
    }

    public String getScoreNumber() 
    {
        return scoreNumber;
    }
    public void setAnalyse(String analyse) 
    {
        this.analyse = analyse;
    }

    public String getAnalyse() 
    {
        return analyse;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userid", getUserid())
            .append("username", getUsername())
            .append("joinTime", getJoinTime())
            .append("favoriteNumber", getFavoriteNumber())
            .append("watchedNumber", getWatchedNumber())
            .append("completePercentage", getCompletePercentage())
            .append("averageScore", getAverageScore())
            .append("standardDeviation", getStandardDeviation())
            .append("scoreNumber", getScoreNumber())
            .append("analyse", getAnalyse())
            .toString();
    }
}
