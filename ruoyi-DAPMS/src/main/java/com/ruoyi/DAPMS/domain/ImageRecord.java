package com.ruoyi.DAPMS.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 图片索引对象
 *
 * @author tianq
 * Date: 2023-10-18
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("image_record")
public class ImageRecord implements Serializable {

    // 图片ID
    @TableId(type = IdType.AUTO)
    private Long imageId;

    // 图片别名，controller中提供api实现别名访问，计划在此处填写parkId
    private String imageAlias;

    // 图片存储目录
    private String storagePath;

    // 图片创建时间
    @JsonFormat(pattern = "yyyy-MM-dd kk:mm:ss")
    private Date createTime;

    // 引用计数，为0时可安全删除
    private Long referenceCount;

    //删除标记
    @TableLogic
    private Long deleted;

}
