package com.ruoyi.DAPMS.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "diagram_cache")
public class DiagramCache {

    @TableId(type = IdType.AUTO)
    private Long cacheId;

    private String subjectId;

    private Long minScore;

    private Long maxScore;

    private Long imageId;

    private String wordFreq;

    @JsonFormat(pattern = "yyyy-MM-dd kk:mm:ss")
    private Date createTime;

    public DiagramCache(String subjectId, int minScore, int maxScore, Long imageId, String wordFreq) {
        this.subjectId = subjectId;
        this.minScore = (long) minScore;
        this.maxScore = (long) maxScore;
        this.imageId = imageId;
        this.wordFreq = wordFreq;
        this.createTime = new Date();
    }
}
