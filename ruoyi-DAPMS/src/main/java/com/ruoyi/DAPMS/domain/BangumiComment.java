package com.ruoyi.DAPMS.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.ruoyi.common.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 展示bangumi评论对象 bangumicomment
 *
 * @author 吴春梅、任容
 * @date 2023-11-17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BangumiComment implements Serializable {
    @TableField(exist = false)
    private String quantity;

    @TableField(exist = false)
    private String chname;
    @TableField(exist = false)
    private String bangumiallscore;
    @TableField(exist = false)
    private String bangumiallnum;
    @TableField(exist = false)
    private String doubanallscore;
    @TableField(exist = false)
    private String doubanallnum;


    /** 作品ID */
    @TableId(type = IdType.AUTO)
    private String id;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 用户ID */
    @Excel(name = "用户ID")
    private String userid;

    /** 评论时间 */
    @Excel(name = "评论时间")
    private String time;

    /** 评论内容 */
    @Excel(name = "评论内容")
    private String comment;

    /** 评分 */
    @Excel(name = "评分")
    private String score;
}
